#!/bin/sh

: ${PG_USER:=postgres}

create_table() {
	psql -U $PG_USER -d $1 -c "CREATE TABLE IF NOT EXISTS $2 ( a integer, b integer, c integer)" && \
	psql -U $PG_USER -d $1 -c "TRUNCATE TABLE $2 RESTART IDENTITY"
}

create_db() {
	psql -U $PG_USER -tc "SELECT 1 FROM pg_database WHERE datname = '$1'" | grep -q 1 || psql -U $PG_USER -c "CREATE DATABASE $1"
}

create_db foo
create_table foo source
create_db bar
create_table bar dest
