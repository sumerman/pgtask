.PHONY: create_db clean run
CC=gcc

CFLAGS += -I $(shell pg_config --includedir) -Wall -Werror=pedantic -ansi
LD_FLAGS += -L $(shell pg_config --libdir) -lpq

run: main create_db
	./main

main: main.c
	${CC} main.c -o main ${CFLAGS} ${LD_FLAGS}

clean:
	-@rm ./main

create_db:
	./create_db.sh
	
