#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <libpq-fe.h>

PGconn *srcConn = NULL;
PGconn *dstConn = NULL;

static void
exit_nicely()
{
	if (srcConn) PQfinish(srcConn);
	if (dstConn) PQfinish(dstConn);
	exit(1);
}

static void
ensure_result(PGconn *conn, PGresult *res, ExecStatusType status)
{
	if (PQresultStatus(res) != status)
	{
		fprintf(stderr, "command failed: %s", PQerrorMessage(conn));
		PQclear(res);
		exit_nicely();
	}
	PQclear(res);
}

static void
ensure_ok(PGconn *conn, PGresult *res)
{
	ensure_result(conn, res, PGRES_COMMAND_OK);
}

static PGconn *
connect(const char *conninfo)
{
	PGconn *conn;

	conn = PQconnectdb(conninfo);
	if (PQstatus(conn) != CONNECTION_OK)
	{
		fprintf(stderr, "Connection to database failed: %s",
				PQerrorMessage(conn));
		exit_nicely();
	}
	return conn;
}

int
main(int argc, char **argv) 
{
	PGresult   *res;
	const char *copyEndReason;
	char       *copyBuff;
	int         copyRes;
	int         copyFromRes;

	srcConn = connect("user=postgres dbname=foo");
	dstConn = connect("user=postgres dbname=bar");

	res = PQexec(srcConn, "INSERT INTO source SELECT a, a%3 AS b, a%5 AS c FROM generate_series(1, 1e6) a");
	ensure_ok(srcConn, res);


	res = PQexec(srcConn, "COPY source TO STDOUT WITH BINARY");
	ensure_result(srcConn, res, PGRES_COPY_OUT);

	res = PQexec(dstConn, "COPY dest FROM STDIN WITH BINARY");
	ensure_result(dstConn, res, PGRES_COPY_IN);

	do 
	{
		copyBuff = NULL;
		copyRes = PQgetCopyData(srcConn, &copyBuff, 0);

		copyFromRes = PQputCopyData(dstConn, copyBuff, copyRes);
		assert(copyFromRes != 0);
		if (copyFromRes < 0)
		{
			copyRes = -2;
			fprintf(stderr, "Copy failed: %s",
					PQerrorMessage(dstConn));
		}

		if (copyBuff != NULL) {
			PQfreemem(copyBuff);
		}
	}
	while (copyRes >= 0);

	if (copyRes == -1) {
		copyEndReason = NULL;
	}
	else
	{
		copyEndReason = "Copy failed!";
	}

	copyFromRes = PQputCopyEnd(dstConn, copyEndReason);
	assert(copyFromRes != 0);

	if (copyFromRes == 1)
	{
		res = PQgetResult(dstConn);
		ensure_ok(dstConn, res);
	}
	else 
	{
		fprintf(stderr, "Copy failed: %s",
				PQerrorMessage(dstConn));
		exit_nicely();
	}

	if (copyRes == -1) 
	{
		res = PQgetResult(srcConn);
		ensure_ok(srcConn, res);
	}
	else 
	{
		fprintf(stderr, "Copy failed: %s",
				PQerrorMessage(srcConn));
		exit_nicely();
	}

	PQfinish(srcConn);
	PQfinish(dstConn);
	return 0;
}
